# otterly adorable

This is a place to find my otterly adorable character I did for a stickerpack as well as all the stickers. Enjoy.

## Can I use this rig
Yes, you are allowed, as long as it's non-commercial and you use the same creative commons lisence for republishing, you can use this asset for free.

You might be dazzled by the amount of controlers and some might now work in intended ways ... I am planning on providing a few tutorials on, how to rig it and, what technices I used to create this creature, but this will need some time.

## I need moar of this otter!
 - [Have a look at a cute animation over reddit.](https://www.reddit.com/r/furry/comments/lp94ka/waking_ott_stylised_3d_animation_using_blender/)
 - [Have a look at the same animation on sketchfab.](https://sketchfab.com/3d-models/otterly-adorable-rigging-showcase-ac9baac90f034a46a1496650caadcd0a)
 - [Get the stickerpack for Telegram!](https://t.me/addstickers/otterly_adorable)
